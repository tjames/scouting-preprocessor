-- align
--
-- Alignment unit.
--
-- D. R. May 2018
library IEEE;
use IEEE.std_logic_1164.all;

library unisim;
use unisim.vcomponents.all;

use work.datatypes.all;

entity align is
    port (
        clk          : in  std_logic;
        rst          : in  std_logic;
        align_marker : in  std_logic;
        d            : in  ldata;
        q            : out ldata
        );
end align;

architecture Behavioral of align is
begin

    -- TODO: Actual alignment

    q <= d;

end Behavioral;
