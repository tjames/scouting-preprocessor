----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/09/2018 09:17:41 AM
-- Design Name: 
-- Module Name: test_file_io - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.all;
use WORK.top_decl.all;
use WORK.datatypes.all;
use STD.TEXTIO.all;

package test_file_io is


procedure ReadFrame (
  file F : text; 
  constant NSTREAMS : in integer;
  variable frame : out ldata
  );
  
  
--procedure DumpEvent (
--  variable event : in TGMTEvent
--  );

end package test_file_io;

package body test_file_io is

  function reverse_bits(a: in std_logic_vector)
    return std_logic_vector
  is
    variable a_rev: std_logic_vector(a'REVERSE_RANGE);
  begin
    for i in a'RANGE loop
      a_rev(i) := a(i);
    end loop;
    return a_rev;
  end;


  procedure ReadFrame (
    file F : text; 
    constant NSTREAMS : in integer;
    variable frame : out ldata
    ) is 
    
    variable L : line;
    variable L1 : line;
    variable vsb : std_logic_vector (3 downto 0);

    variable dummy : string (1 to 1);
    variable bDone : boolean := false;
  begin
    bDone := False;
    -- read event
    while ( not endfile(F) and not bDone) loop
      readline (F, L);
      write (L1,string'("read line:"));
      write (L1, L.all);
      writeline(OUTPUT,L1 );
      if (L.all(1 to 2) = "--" ) then
        next;
      end if;
      for i in 0 to NSTREAMS - 1 loop
         hread(L, frame(i).data); 
         read(L, dummy);   
         bread(L, vsb);
         if not (i = NSTREAMS-1) then
           read(L, dummy);   
         end if;
         frame(i).valid := vsb(3);
         frame(i).strobe := vsb(2);
 --        frame(i).bx_start := vsb(1); 
         frame(i).done := vsb(0);
         bDone := true;
      end loop;
      
    end loop;  

  end ReadFrame;

/*
  procedure DumpEvent (
    variable event : in TGMTEvent
    ) is 
    
    variable L1 : line;

  begin
    for i in 0 to 3 loop
      write (L1,string'("DT   Muon "));
      write (L1,i+1);
      write (L1,string'(" :"));
      write (L1,event.DTMuons(i));
      writeline (OUTPUT, L1);
    end loop;  -- i
    for i in 0 to 3 loop
      write (L1,string'("CSC  Muon "));
      write (L1,i+1);
      write (L1,string'(" :"));
      write (L1,event.CSCMuons(i));
      writeline (OUTPUT, L1);
    end loop;  -- i
    for i in 0 to 3 loop
      write (L1,string'("BRPC Muon "));
      write (L1,i+1);
      write (L1,string'(" :"));
      write (L1,event.bRPCMuons(i));
      writeline (OUTPUT, L1);
    end loop;  -- i
    for i in 0 to 3 loop
      write (L1,string'("FRPC Muon "));
      write (L1,i+1);
      write (L1,string'(" :"));
      write (L1,event.fRPCMuons(i));
      writeline (OUTPUT, L1);
    end loop;  -- i      
    write (L1, string'("MIP bits : "));
    writeline (OUTPUT, L1);
    for i in 0 to 13 loop
      write (L1, TO_BITVECTOR ( event.MIPbits(i) ));
      writeline (OUTPUT, L1);
    end loop;
    write (L1, string'("Quiet bits : "));
    writeline (OUTPUT, L1);
    for i in 0 to 13 loop
      write (L1, TO_BITVECTOR ( event.Quietbits(i) ));
      writeline (OUTPUT, L1);
    end loop;
    write (L1, string'("PHI SELECTBITS:"));
    for i in 0 to 31 loop
      write (L1, string'(" "));
      write (L1, TO_INTEGER (UNSIGNED(event.PhiSelBits(i))));
    end loop;  -- i
    writeline (OUTPUT, L1);
    write (L1, string'("ETA SELECTBITS:"));
    for i in 0 to 31 loop
      write (L1, string'(" "));
      write (L1, TO_INTEGER (UNSIGNED(event.EtaSelBits(i))));
    end loop;  -- i
    writeline (OUTPUT, L1);
    for i in 0 to 3 loop
      write (L1,string'("GMT  Muon "));
      write (L1,i+1);
      write (L1,string'(" :"));
      write (L1,event.GMTMuons(i));
      writeline (OUTPUT, L1);
    end loop;  -- i
    for i in 0 to 5 loop
      write (L1,string'("Pair Matrix "));
      write (L1,i);
      write (L1,string'(" :"));
      for r in 0 to 3 loop
        for c in 0 to 3 loop
          write (L1,string'(" "));
          write (L1, TO_BIT(event.PairMatrices(i)(r,c)));
        end loop;  -- c
      end loop;  -- r
      writeline (OUTPUT, L1);      
    end loop;  -- i
    for i in 0 to 5 loop
      write (L1,string'("MQ Matrix "));
      write (L1,i);
      write (L1,string'(" :"));
      for r in 0 to 3 loop
        for c in 0 to 3 loop
          write (L1,string'(" "));
          write (L1, TO_BITVECTOR(event.MQMatrices(i)(r,c)));
        end loop;  -- c
      end loop;  -- r
      writeline (OUTPUT, L1);      
    end loop;  -- i
    for i in 0 to 3 loop
      write (L1,string'("CANCEL Bits "));
      write (L1,i);
      write (L1,string'(" :"));
      write (L1, TO_BITVECTOR(event.CancelBits(i)));
      writeline (OUTPUT, L1);
    end loop;  -- i
    
  end DumpEvent;
 */ 
end package body test_file_io;






