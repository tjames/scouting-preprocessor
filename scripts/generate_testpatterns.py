#!/usr/bin/env python3
import click
import random

COMMA = 0x505050BC
PADDING = 0x7F7F7F7F

example_muons = [0x39a1948d0e64695f, 0x3c811c870f24196b, 0x2ae11c390aa1a92b, 0x2ae054830aa4d95f
        , 0x2d611c740b43812b, 0x31e0546c0c64215f, 0x3762d4580dc2c12b, 0x2ae0847d0aa3892b
        , 0x3c84646f0f23793b, 0x39a0cc660e62f12b, 0x3764647e0dc3f15f, 0x3c80348e0f25516f
        ]

def get_first_or_second_word(data, part):
    mask = 2**32-1 << 32*(part-1)
    return (data & mask) >> 32*(part-1)

def create_link_word(evt_word, counter, bcounter_in_clocks, orbit_counter, orbit_delay_in_clocks, padding, comma, no_counters, non_counter_data):
    single_link_word_pattern = []
    if comma: 
        data = COMMA
        valid = '0'
    else:
        if not no_counters:
            if evt_word == 0:
                data = bcounter_in_clocks//6+1
            elif evt_word == 1:
                data = orbit_counter
            elif evt_word == 2:
                data = get_first_or_second_word(non_counter_data[0], 2) 
            elif evt_word == 3:
                data = get_first_or_second_word(non_counter_data[0], 1)
            elif evt_word == 4:
                data = get_first_or_second_word(non_counter_data[1], 2) 
            elif evt_word == 5:
                data = get_first_or_second_word(non_counter_data[1], 1)
        else:
            data = bcounter_in_clocks
        valid = '1'
    # IMPORTANT: Padding word takes precendence (i.e. overwrites data word from previous step)
    if padding:
        data = PADDING
        strobe = '0'
    else:
        strobe = '1'
    last = '0'
    start = '0'

    single_link_word_pattern.append(valid)
    single_link_word_pattern.append(strobe)
    single_link_word_pattern.append(start)
    single_link_word_pattern.append(last)

    return "{0:0{1}x}".format(data, 8), single_link_word_pattern


@click.command()
@click.argument('outfile', type=click.File('w'))
@click.option('--total_words', '-t', type=int, default=450, show_default=True)
@click.option('--num_links', '-n', type=int, default=2, show_default=True)
@click.option('--orbit_length', '-l', type=int, default=60, show_default=True)
@click.option('--orbit_delay', '-d', type=int, default=10, show_default=True)
@click.option('--gap_length', '-g', type=int, default=5, show_default=True)
@click.option('--percent_padding', type=click.IntRange(0, 100), default=4, show_default=True)
@click.option('--no_counters', '-c', is_flag=True, default=False)
@click.option('--links_aligned', '-a', is_flag=True, default=False)
@click.option('--padding_words_aligned', '-p', is_flag=True, default=False)
@click.option('--max_desync', '-m', type=int, default=32, show_default=True)
@click.option('--percent_muons', type=click.IntRange(0, 100), default=15, show_default=True)
@click.option('--dummy_data', is_flag=True, default=False)
@click.option('--debug', is_flag=True, default=False)
def main(outfile, num_links, total_words, orbit_length, orbit_delay, gap_length, no_counters, links_aligned, padding_words_aligned, percent_padding, max_desync, percent_muons, dummy_data, debug):
    info_string = []
    warning_string = []
    info_string.append("Generating data for {} clock cycles on {} links.".format(total_words, num_links))
    info_string.append("The orbit length is {} and we will start generating {} bunch crossings before BX0. The comma gap is {}.".format(orbit_length, orbit_delay, gap_length))
    if not dummy_data:
        info_string.append("About {} percent of slots are filled with muons.".format(percent_muons))
    else:
        info_string.append("The non-counter data are dummy values.")

    # Make a counter for each link
    if links_aligned:
        max_desync = 0
    counters = [random.randint(-1*max_desync, 0) for i in range(num_links)]

    if not links_aligned:
        info_string.append("Links are desynchronised by a maximum of {} clocks. (Actual relative delays: {})".format(max_desync, counters))
    else:
        info_string.append("Links are generated as aligned.")
    info_string.append("About {} percent of all generated words will be padding.".format(percent_padding))
    if padding_words_aligned and not links_aligned:
        links_aligned = True
        warning_string.append("[WARNING] Padding alignment requested without also requesting aligned links. These options are incompatible, so link alignment switched ON!")
    elif padding_words_aligned:
        info_string.append("Padding words aligned and outside of bunch crossings.")
    print('\n'.join(info_string))
    print('\n'.join(warning_string))

    orbit_length_in_clocks = 6*orbit_length
    orbit_delay_in_clocks = 6*orbit_delay
    gap_length_in_clocks = 6*gap_length
    bcounters_in_clocks = [orbit_length_in_clocks+count-orbit_delay_in_clocks for count in counters]
    evt_words = [(i-1)%6 for i in bcounters_in_clocks] # Because we're starting to count at 1 for the bcounter
    ocounters = num_links*[0]
    non_counter_words = num_links*[[0, 0]] 
    for i in range(total_words):
        ## Create bunch and orbit counters as well as padding and comma deciders
        if not links_aligned and not padding_words_aligned:
            padding_decision = list(map(lambda x: random.randint(0, 1000) < percent_padding*10, range(num_links))) 
        elif evt_words[0] == 5: # Links are aligned so evt_word is the same for each channel
            padding_decision = num_links*[random.randint(0, 1000) < percent_padding*10]
        else:
            padding_decision = num_links*[False]

        comma_decision = num_links*[False] 

        for j in range(len(counters)):
            if not padding_decision[j]:
                counters[j] += 1
                if evt_words[j] < 5:
                    evt_words[j] += 1
                else:
                    evt_words[j] = 0
                if bcounters_in_clocks[j] < orbit_length_in_clocks:
                    bcounters_in_clocks[j] += 1
                else:
                    bcounters_in_clocks[j] = 1
                    ocounters[j] += 1
            if (orbit_length_in_clocks-bcounters_in_clocks[j]) < gap_length_in_clocks:
                comma_decision[j] = True
            else:
                comma_decision[j] = False
            if evt_words[j] == 0:
                muon_probability = random.random()
                if not dummy_data:
                    if muon_probability < percent_muons/100:
                        non_counter_words[j] = [random.choice(example_muons), 0]
                    if muon_probability < (percent_muons/100)**2:
                        non_counter_words[j] = [random.choice(example_muons), random.choice(example_muons)]
                else:
                    non_counter_words[j] = [0xCAFECAFEBEEFBEEF, 0xCAFECAFEBEEFBEEF]

        if debug:
            outfile.write("{} ".format(comma_decision))
            outfile.write("{} ".format(bcounters_in_clocks))
            outfile.write("{} ".format(counters))
            outfile.write("{}  ".format(evt_words))
            outfile.write("{}  ".format(non_counter_words))

        single_word_pattern = []
        for linkID in range(len(counters)):
            data, single_link_word_pattern = create_link_word(evt_words[linkID], counters[linkID], bcounters_in_clocks[linkID], ocounters[linkID], orbit_delay_in_clocks, padding_decision[linkID], comma_decision[linkID], no_counters, non_counter_words[linkID])
            single_word_pattern.append(' '.join([data, ''.join(single_link_word_pattern)]))
        outfile.write(' '.join(single_word_pattern)+'\n')

if __name__ == '__main__':
    main()
